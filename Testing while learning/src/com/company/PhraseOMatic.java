package com.company;

public class PhraseOMatic {
    public static void main(String[] args){
        String[] wordList1 = {"Eikime", "Neikime", "Kodel", "Nesakykite"};
        String[] wordList2 = {" namo ", " i darba ", " parduotuven ", " niekur "};
        String[] wordList3 = {"siandien", "rytoj", "o gal kazkada", "o gal niekada"};

        int firstLength = wordList1.length;
        int secondLength = wordList2.length;
        int thirdLLength = wordList3.length;

        int rand1 = (int) (Math.random() * firstLength);
        int rand2 = (int) (Math.random() * secondLength);
        int rand3 = (int) (Math.random() * thirdLLength);

        String phrase = wordList1[rand1] + " " + wordList2[rand2] + " "+ wordList3[rand3];
        System.out.println(phrase);
    }
}
