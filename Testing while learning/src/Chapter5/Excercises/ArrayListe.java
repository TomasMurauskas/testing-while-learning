package Chapter5.Excercises;

import java.util.ArrayList;

public class ArrayListe {
    public static void main(String[] args) {
        ArrayList<Egg> myList = new ArrayList<Egg>();
        Egg s = new Egg();

        myList.add(s);

        Egg b = new Egg();

        myList.add(b);

        int theSize = myList.size();

        String[] myList1 = new String[2];
        String a = new String("woohoo");
        myList1[0] = a;
        String d = new String("Frog");
        myList1[1] = d;

        int size = myList1.length;
        System.out.println(size);

//            Object o = myList1[1];
//            System.out.println(o);


//            boolean isIn = myList1.contains(b);
        boolean isIn = false;
        for (String element : myList1) {
            if (d.equals(element)) {
                isIn = true;
                break;
            }
        }
        System.out.println(isIn);


    }
}


