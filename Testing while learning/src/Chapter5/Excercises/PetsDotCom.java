package Chapter5.Excercises;

import java.util.ArrayList;

public class PetsDotCom {
    private String name = "PetsDotCom";
    public void setName(String Name){
        name = Name;
    }
    private ArrayList<String> locationCells;


    public void setLocationCells(ArrayList<String> locs) {
        locationCells = locs;
    }

    public String checkYourself(String userInput) {
        String result = "miss";

        int index = locationCells.indexOf(userInput);
        if (index >= 0) {
            locationCells.remove(index);

            if (locationCells.isEmpty()) {
                result = "kill";
            } else {
                result = "hit";
            }
        }
        return result;
    }

    ;
}
