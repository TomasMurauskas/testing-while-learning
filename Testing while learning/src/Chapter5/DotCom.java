package Chapter5;

import java.util.ArrayList;
import java.lang.*;

public class DotCom {
    public void setLocationCells(ArrayList<String> loc) {
        locationCells = loc;
    }
    private ArrayList<String> locationCells = new ArrayList<>();
    private String name;



    public void setName(String n) {
        name = n;
    }

    public String checkYourself(String userInput) {
        String result = "miss";

        int index = locationCells.indexOf(userInput);
        if (index >= 0) {
            locationCells.remove(index);

            if (locationCells.isEmpty()) {
                result = "kill";
                System.out.println("Ouch! You sunk " + name + " : ( ");
            } else {
                result = "hit";
            }
        }
        return result;
    }
}
