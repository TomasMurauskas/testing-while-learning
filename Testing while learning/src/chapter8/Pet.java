package chapter8;

public abstract interface Pet{
    public abstract void beFriendly();  //interface methods have no implementation body
    public abstract void play(); //interface methods have no implementation body

}
