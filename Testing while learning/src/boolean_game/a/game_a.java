package boolean_game.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class game_a {

   public void run() {
       var lvlStr = getUserInput("Enter difficulty");
       int randomNumber = getRandomNumber(Integer.parseInt(lvlStr));

        System.out.println("Your random number was " + randomNumber);
        int guess = getUserGuess();

        while (guess != randomNumber) {
            System.out.println("Guess again!");
            guess = getUserGuess();
        }
        System.out.println("You guess right. WIN!");
    }

    private int getRandomNumber(int max) {
        return (int) (Math.random() * max);
    }

    private int getUserGuess() {
        var stringGuess = getUserInput("Enter your guess");
        System.out.println("You guessed " + stringGuess);
        return Integer.parseInt(stringGuess);
    }

    private String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + "  ");
        try {
            BufferedReader is = new BufferedReader(
                    new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        } catch (IOException e) {
            System.out.println("IOException: error " + e);
        }
        return inputLine.toLowerCase();
    }
}
