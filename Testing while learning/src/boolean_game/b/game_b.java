package boolean_game.b;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class game_b {

    private boolean[] newGameArray;
    private int hitsToSinkTheShip = 0;
    int numOfGuesses = 0;
    private boolean[] storedHitsArray;



    public void run() {

        var lvlStr = getUserInput("Enter difficulty from 0 to n"); //enter difficulty
        int intLvlStr = Integer.parseInt(lvlStr);
        makeGameBoard(intLvlStr);

        while (hitsToSinkTheShip < 3) {

            int guess = getUserGuess(); // we assign our input guess to guess variable

            boolean [] storedHitsArray = new boolean[intLvlStr];

            if (newGameArray[guess] == false) {
                System.out.println("Miss");
                numOfGuesses++;
                if (newGameArray[guess] == storedHitsArray[guess]){
                System.out.println("You already tried this coordinate");
                }
            }
            else {
                System.out.println("Hit");

                System.out.println(newGameArray[guess]);
                storedHitsArray[guess] = newGameArray[guess];
                newGameArray[guess] = false;
                hitsToSinkTheShip++;
                numOfGuesses++;

                }
            }

        System.out.println("WIN! Ship was sunk!");
        System.out.println("You took " + numOfGuesses + " guesses to sink the ship");
//
    }

    public void makeGameBoard(int intLvlStr) {
        newGameArray = new boolean[intLvlStr];  //we create an empty array instance of game_b, with intStr number of possible places
        for (boolean isHitLocation : newGameArray) { //loops through array
            isHitLocation = false;  //we assign values of false to each.
//            System.out.println(isHitLocation);
        }

        int startingLocation = (int) (Math.random() * (intLvlStr - 2)); //random first position of guessing locations , e.g. 2,3,4
        System.out.println("Your random location was " + startingLocation);
        for (int i = (startingLocation); i < (startingLocation + 3); i++) {
            newGameArray[i] = true;
        }
        for (boolean isHitLocation : newGameArray) {
            System.out.println(isHitLocation);
        }

    }

    private int getRandomNumber(int max) {
        return (int) (Math.random() * max);
    }

    private int getUserGuess() {
        var stringGuess = getUserInput("Enter your guess");
        System.out.println("You guessed " + stringGuess);
        return Integer.parseInt(stringGuess);
    }

    private String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + "  ");
        try {
            BufferedReader is = new BufferedReader(
                    new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        } catch (IOException e) {
            System.out.println("IOException: error " + e);
        }
        return inputLine.toLowerCase();
    }
}
