package boolean_game.c;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Proxy;
import java.sql.SQLOutput;


public class game_c {


    private int numberOfRows = 10;
    private int numberOfCol = 10;
    Cell[][] gameMatrix = makeGameBoard(numberOfRows, numberOfCol);
    Cell[][] guessGameMatrix = makeGameBoard(numberOfRows, numberOfCol);
    int allShipsWereHit = 0;
    int occupiedFields = 0;

    public void run() {


    }

    public Cell[][] makeGameBoard(int numOfRow, int numOfCol) {
        numberOfCol = numOfCol;
        numberOfRows = numOfRow;
        Cell[][] gameMatrix = new Cell[numOfRow][numOfCol];

        for (int x = 0; x < numOfRow; x++) {
            for (int y = 0; y < numOfCol; y++) {
                var cell = new Cell(x, y);
                gameMatrix[x][y] = cell;

            }
        }
        return gameMatrix;
    }


    public void placeShipsOnTheGameBoard() {

        int randomX;
        int randomY;

        int randomDirectionGenerator = (int) (Math.random() * 2); // 1 is vertical; 0 is horizontal;
        System.out.println("direction " + randomDirectionGenerator);
        if (randomDirectionGenerator == 1) {
            randomX = (int) (Math.random() * 5);
            System.out.println(randomX + " test");
            randomY = 1; //width 1
            System.out.println(randomY + " test");
            for (int x = randomX; x < (1 + randomX + randomX); x++) {
                for (int y = randomY; y < (1 + randomY + randomY); y++) {
                    gameMatrix[x][y].isShipPresent = true;
                    occupiedFields++;
//                            gameMatrix[x][y].printOutGame();
                }
            }

        } else {
            randomX = 1;
            System.out.println(randomX + " test2");
            randomY = (int) (Math.random() * 5);

            for (int x = randomX; x < (1 + randomX + randomX); x++) {
                for (int y = randomY; y < (1 + randomY + randomY); y++) {
                    gameMatrix[x][y].isShipPresent = true;
                    occupiedFields++;
                }
            }
        } //end of ship assignment loop

        System.out.println("     0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9   ");
        System.out.println("   ------------------------------------------------------------");
        for (int x = 0; x < numberOfCol; x++) {  //loop to print out assigned matrix
            for (int y = 0; y < numberOfRows; y++) {

                gameMatrix[x][y].printOutGame(x, y);
            }
            System.out.println();
        }
        System.out.println("--------------------END OF ASSIGNMENT---------------------");
    }

    public Cell getUserGuess() {
        var stringGuess = getUserInput("Enter your vertical guess from 0 to 9");
//        System.out.println("You guessed " + stringGuess);
        int x = Integer.parseInt(stringGuess);
        stringGuess = getUserInput("Enter your horizontal guess from 0 to 9");
        int y = Integer.parseInt(stringGuess);
        return gameMatrix[x][y];

    }

    private String getUserInput(String prompt) {
        String inputLine = null;
        System.out.print(prompt + "  ");
        try {
            BufferedReader is = new BufferedReader(
                new InputStreamReader(System.in));
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        } catch (IOException e) {
            System.out.println("IOException: error " + e);
        }
        return inputLine.toLowerCase();
    }

    public void checkUserGuess() throws Exception {
        try { while (occupiedFields != allShipsWereHit) {
            Cell guessLocation = getUserGuess();
            if (guessLocation.isShipPresent == true) {

                System.out.println("HIT. You hit the ship");
                guessGameMatrix[guessLocation.x][guessLocation.y] = guessLocation;
                gameMatrix[guessLocation.x][guessLocation.y].isShipPresent = false;
                guessGameMatrix[guessLocation.x][guessLocation.y].isShipPresent = true;

                allShipsWereHit++;
//                System.out.println(guessGameMatrix[guessLocation.x][guessLocation.y].isShipPresent + " cell in guessmatrix2");

                System.out.println("     0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9   ");
                System.out.println("   ------------------------------------------------------------");
                for (int x = 0; x < numberOfRows; x++) {
                    for (int y = 0; y < numberOfCol; y++) {
                        guessGameMatrix[x][y].printOutGame(x, y);
                    }
                    System.out.println();
                }

                System.out.println("     0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9   ");
                System.out.println("   ------------------------------------------------------------");
                for (int x = 0; x < numberOfRows; x++) {
                    for (int y = 0; y < numberOfCol; y++) {
                        gameMatrix[x][y].printOutGame(x, y);
                    }
                    System.out.println();
                }
            } else {
                System.out.println("MISS. TRY AGAIN");
            }
        }
        System.out.println("WIN. All of the ships were sunk!");
    } catch (Exception se) {
            System.out.println("Please enter a number (not a letter) in the given range");
            checkUserGuess();
        }
    }
}
