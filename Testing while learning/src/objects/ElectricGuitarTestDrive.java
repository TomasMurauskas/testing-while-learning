package objects;

public class ElectricGuitarTestDrive {
    public static void main(String[] args){

        ElectricGuitar one = new ElectricGuitar();

        one.setBrand("versace");
        System.out.println(one.getBrand());

        one.setNumOfPickups(15);
        System.out.println(one.getNumberOfPickups());

        one.setRockStarUsesIt(true);
        System.out.println(one.getRockStarUsesIt());
    }
}
