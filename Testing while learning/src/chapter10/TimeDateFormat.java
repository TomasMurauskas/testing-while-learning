package chapter10;

import java.util.Calendar;
import java.util.Date;

public class TimeDateFormat {

    public static void main(String[] args){
        Calendar calndr = Calendar.getInstance();

        System.out.println("Current Date: "+ calndr.getTime());

        calndr.add(Calendar.YEAR, 50);

        System.out.println("After 50 days: "
                + calndr.getTime());

    }
}
