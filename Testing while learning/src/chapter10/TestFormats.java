package chapter10;

public class TestFormats {
    public static void main(String[] args) {

        // %[ argument number] [flags][width][.precision] types


        String s = String.format("I have %.2f bugs to fix.", 10000000.536000);
        System.out.println(s);
    }
}
