package chapter10;

public class WrapperMagic {
    Integer i =1;
    int j;

    public static void main(String[] args){
        WrapperMagic t = new WrapperMagic();
        t.go();

        double d = 42.5;
        String doubleString = "" + d;

        double e = 42.5;
        String doubleStringe = Double.toString(e);

    }
    public void go() {

        double d = 42.5;
        String doubleString = "" + d;

        double e = 42.5;
        String doubleStringe = Double.toString(e);

//        WrapperMagic doubleString = new WrapperMagic();
//        WrapperMagic doubleStringe = new WrapperMagic();
        j=i;
        System.out.println(j);
        System.out.println(i);
        System.out.println(doubleString);
        System.out.println(doubleStringe);
        String tellTheClass = doubleString.getClass().getName();
        System.out.println(tellTheClass);

        String tellTheClasse = doubleStringe.getClass().getName();
        System.out.println(tellTheClass);


    }
}
