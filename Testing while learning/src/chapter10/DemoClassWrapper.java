package chapter10;

import java.util.ArrayList;

public class DemoClassWrapper {
//    9.99 - int, float, double, char
    //Integer, FLoat, Double, Character

    public static void main(String[] args) throws Exception
    {

        int i = 5;
        Integer ii = new Integer(i); //Boxing..
        Integer jj = i;              //Autoboxing

        int j = jj.intValue();      //unboxing
        int k =jj;                  //autoUnboxing

        ArrayList<Integer> values = new ArrayList<Integer>();
        values.add(5);
        values.add(7);
//        values.add("Navin");  not allowed

    }
}
