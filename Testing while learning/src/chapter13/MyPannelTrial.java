package chapter13;

import javax.swing.*;
import java.awt.*;

public class MyPannelTrial {
    public static void main(String[] args) {
//        JFrame frame = new JFrame();
//        frame.setVisible(true);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setSize(200, 200);
//
//        JButton button = new JButton("There is no spoon....");
//        Font bigFont = new Font("serif", Font.BOLD, 28);
//        button.setFont(bigFont);
//        frame.getContentPane().add(BorderLayout.NORTH, button);

        // ITS AN EXAMPLE HOW PANEL IS FORMED USING BORDERLAYOUT

//        JFrame frame = new JFrame();
//
//        JButton east = new JButton("East");
//        JButton west = new JButton("West");
//        JButton north = new JButton("North");
//        JButton south = new JButton("South");
//        JButton center = new JButton("Center");
//
//        frame.getContentPane().add(BorderLayout.EAST, east);
//        frame.getContentPane().add(BorderLayout.WEST, west);
//        frame.getContentPane().add(BorderLayout.NORTH, north);
//        frame.getContentPane().add(BorderLayout.SOUTH, south);
//        frame.getContentPane().add(BorderLayout.CENTER, center);
//
//        frame.setSize(300,300);
//        frame.setVisible(true);

        // NEW EXAMPLE BELOW FLOW LAYOUT

        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        panel.setBackground(Color.darkGray);
        frame.getContentPane().add(BorderLayout.EAST, panel);
        frame.setSize(200, 200);
        frame.setVisible(true);

        JButton button = new JButton("shock me");
        JButton button2 = new JButton("shock me twice");
        JButton button3 = new JButton("shock me thrice");

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(button);
        panel.add(button2);
        panel.add(button3);
        frame.getContentPane().add(BorderLayout.EAST, panel);

        frame.setSize(250, 200);
        frame.setVisible(true);


    }


}
