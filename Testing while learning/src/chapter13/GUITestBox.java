package chapter13;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class GUITestBox implements ActionListener {
    JTextArea text;
    JCheckBox check;

    public static void main(String[] args) {
        GUITestBox gui = new GUITestBox();
        gui.go();
    }
    public void itemStateChanged(ItemEvent ev) {
        String onOrOff = "off";
        if(check.isSelected()) onOrOff = "on";
        System.out.println("Check box is " +onOrOff);
    };

    public void go() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        JButton button = new JButton("Just Click it");
        button.addActionListener(this);
        text = new JTextArea(10,20);
        text.setLineWrap(true);

        JScrollPane scroller = new JScrollPane(text);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        panel.add(scroller);

        check = new JCheckBox("goes to 11");
        panel.add(check);
        check.addItemListener(this::itemStateChanged);




        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.getContentPane().add(BorderLayout.SOUTH, button);

        frame.setSize(350, 300);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent ev) {
        text.append("button clicked \n ");
    }
}
