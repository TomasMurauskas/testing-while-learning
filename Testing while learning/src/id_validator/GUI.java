package id_validator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GUI implements ActionListener {

    JButton button;
    JLabel label;
    JPanel panel;
    JTextField textField;
    boolean ID = false;


    public GUI() {
        JFrame frame = new JFrame();

        button = new JButton("Validate");
        button.setFont(new Font("Arial", Font.BOLD, 18));
        button.addActionListener(this);
        label = new JLabel("Enter the ID");
        label.setFont(new Font("Arial", Font.BOLD, 18));


        textField = new JTextField();
        textField.setPreferredSize(new Dimension(140, 30));

        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(100, 100, 60, 100));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(button);
        panel.add(label);
        panel.add(textField);


        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("ID Validator");
        frame.pack();
        frame.setVisible(true);

        frame.getContentPane().add(BorderLayout.SOUTH, button);
    }


    public static void main(String[] args) {
       new GUI();//TODO main code
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        long input = Long.parseLong(textField.getText());

        TestTheID testTheID = new TestTheID(); //create an obj to get its number's digits
        //ID digits are assigned a long from A to K. first letter is 11, last - 1. Reverse order though;
        long K = testTheID.getDigitOfNumber(input, 1); // allowed: [1;6]
        long J = testTheID.getDigitOfNumber(input, 2); // allowed: [0;9] no check
        long I = testTheID.getDigitOfNumber(input, 3); // allowed: [0;9] no check
        long H = testTheID.getDigitOfNumber(input, 4); // allowed: [0;1]
        long G = testTheID.getDigitOfNumber(input, 5); // allowed: if D=0 ->[0;9] || if D = 1 -> [0;1]
        long F = testTheID.getDigitOfNumber(input, 6); // allowed: [0;3]
        long E = testTheID.getDigitOfNumber(input, 7); // allowed: if F = [0:2] -> [0;9] || F = 3 [0;1]
        long D = testTheID.getDigitOfNumber(input, 8); // allowed: [0;9] no check
        long C = testTheID.getDigitOfNumber(input, 9); // allowed: [0;9] no check
        long B = testTheID.getDigitOfNumber(input, 10); // allowed: [0;9] no check
        long A = testTheID.getDigitOfNumber(input, 11); // allowed: [checkSum] //TODO

        long checkSum = A * 1 + B * 2 + C * 3 + D * 4 + E * 5 + F * 6 + G * 7 + H * 8 + I * 9 + J * 1;
        long controlNumber = 0;
        if (checkSum % 11 != 10) {
            controlNumber = checkSum % 11;
        } else {
            checkSum = A * 3 + B * 4 + C * 5 + D * 6 + E * 7 + F * 8 + G * 9 + H * 1 + I * 2 + J * 3;
            if (checkSum % 11 != 10) {
                controlNumber = checkSum % 11;
            }
        }
//        System.out.println(A);
//        System.out.println(B);
//        System.out.println(C);
//        System.out.println(D);
//        System.out.println(E);
//        System.out.println(F);
//        System.out.println(G);
//        System.out.println(H);
//        System.out.println(I);
//        System.out.println(J);
//        System.out.println(K);

        //test required numbers starting from A digit:
        ID = true;
//        System.out.println(ID);

        if (A > 6) { ID = false; if (D > 1) {ID = false;}};
        if (D > 1) {ID = false;}
        if (D == 1) { if (E > 1) { ID = false;}}
        if (F > 3) {ID = false;}
        if (F == 3) {if (G > 1) {ID = false;}}
        if (K != controlNumber) {ID = false;}


            if (ID) {
                System.out.println(ID);
                label.setText("ID is valid");
            } else {
                label.setText("ID is invalid");

            }
    } //close actionPerformed
} //close GUI class
