package id_validator;

import java.util.Scanner;

public class TestTheID {

    public static long getDigitOfNumber(long input, int position) {
        int count = 0;
        int remainder = 0;
        int length = Long.toString(input).length();
        if (length == 1) {
            return input;
        } else
        {
            while(input > 0){
                remainder = Math.toIntExact(input % 10);
                count++;
                input=input/10;
                if(count==position)
                {
                    break;
                }
            } return remainder;
        }
    }

    public static void main(String[] args)
    {
        long num;
        long digitOfWantedPosition;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number");
        num=sc.nextLong();
        digitOfWantedPosition = getDigitOfNumber(num, 4);
        System.out.println(digitOfWantedPosition);
    }
}
