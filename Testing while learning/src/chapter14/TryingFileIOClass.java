package chapter14;

import java.io.File;

public class TryingFileIOClass {
    public static void main(String[] args) {
        File f = new File("Mycode.txt");

        File dir = new File("Chapter 15");
        dir.mkdir();

        if(dir.isDirectory()) {
            String[] dirContents = dir.list();
            for (int i = 0; i < dirContents.length; i++) {
                System.out.println(dirContents[i]);
            }
        }
        System.out.println(dir.getAbsolutePath());

        boolean isDeleted = dir.delete();
        System.out.println(isDeleted);
    }
}
