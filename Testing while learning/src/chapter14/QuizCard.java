package chapter14;

import java.util.ArrayList;

public class QuizCard {
     private String question;
     private String answer;
    private ArrayList<QuizCard> cardList;

   public QuizCard(String q, String a) {
        question = q;
        answer = a;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}
