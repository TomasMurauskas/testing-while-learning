package chapter14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReadAFile {
    public static void main(String[] args) {
        try {

            File myFile = new File("MyText.txt"); // is new object created here or not??

            FileReader fileReader = new FileReader(myFile);

            BufferedReader reader =  new BufferedReader(fileReader);

            String line = null;

                while((line = reader.readLine()) != null) {
                System.out.println(line);
                }
            reader.close();

        } catch (Exception ex) {
            System.out.println("an error has occurred");
            ex.printStackTrace();
        }
    }
}
