package chapter9;

public class Duck {
    int size;

    public Duck() {
        size = 15;
    }
    public Duck(int duckSize) {
        if (duckSize == 0) {
            size = 27;
        } else {
            size = duckSize;
        }
    }



    public void getSize() {
        System.out.println(size);
    }
    public void setSize(int duckSize) {
        size = duckSize;
    }


}
