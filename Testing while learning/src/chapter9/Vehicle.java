package chapter9;

public class Vehicle {
    int maxSpeed;

    Vehicle() {
        System.out.println("Vehicle constructor");
    }

    Vehicle(int maxSpeed) {
        System.out.println("Vehicle constructor w/ speed");
        this.maxSpeed = maxSpeed;
    }
}
