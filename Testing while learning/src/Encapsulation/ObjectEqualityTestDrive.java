package Encapsulation;

public class ObjectEqualityTestDrive {
    public static void main(String[] args){
        ObjectEquality one = new ObjectEquality();

        System.out.println(one.a);
        System.out.println(one.b);

        one.a = 4;
        one.b = 6;

        one.comparison(4, (byte) 6);
        System.out.println(one.returnComparison());

    }
}
