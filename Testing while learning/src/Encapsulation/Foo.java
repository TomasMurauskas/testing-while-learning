package Encapsulation;

public class Foo {
    public void go() {
        int x = 3; //x is not initialized unless we give it value
        int z = x + 3;
    }
}


//Method ARGUMENTS are always initialized, e.g. go(int miles)