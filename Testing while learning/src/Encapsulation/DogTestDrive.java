package Encapsulation;

public class DogTestDrive {
    public static void main(String[] args){
        Dog[] pets;
        pets = new Dog[7];

        pets[0] = new Dog();
        pets[1] = new Dog();

        pets[0].setSize(30);
        int x = pets[0].getSize();
        System.out.println(x);
    //The way of reaching variables in object arrays...

    }
}
