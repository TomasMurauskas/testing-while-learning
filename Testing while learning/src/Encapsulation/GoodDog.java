package Encapsulation;

public class GoodDog {
    private int size;
    //all int variables private

    public void setSize(int s){
        size = s;
//        setters and getters are all public
        // All encapsulated data methods should have a declared type which they operate on.

    }
    public int getSize(){
        return size;
    }

    void bark(){
        if (size >60){
            System.out.println("Wooof Woof!");
        }
        else if(size> 14){
            System.out.println("Ruff Ruff");
        }
        else {
            System.out.println("Yip, Yip!!!");
        }
    }
}
