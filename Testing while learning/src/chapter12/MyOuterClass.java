package chapter12;

public class MyOuterClass {
    private int x = 0;

    MyInner inner = new MyInner();
    public void doStuff() {
        inner.go();
    }
    class MyInner {
        void go() {
            x=42;
        }
    } //close Inner
} //close Outer
