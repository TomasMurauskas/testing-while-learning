package chapter7.inheritance;

public class FamilyDoctor extends Doctor {
    boolean makesHouseCalls;

      void giveAdvice() {
        System.out.println("FamilyDoctor was sleepy");
    }


    public static void main(String[] args) {
        FamilyDoctor Andrew = new FamilyDoctor();
        Andrew.giveAdvice();
//        Andrew.treatPatient();   wont work cause it private. only public is inherited

    }
}