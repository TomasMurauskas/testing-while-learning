package chapter7.inheritance;

public class Doctor {
    boolean worksAtHospital;

    private void treatPatient() {
        System.out.println("Doctor spoke");
    }

    public static void main(String[] args) {
        Doctor Andrew = new Doctor();

        Andrew.treatPatient();
    }
}
