package chapter7.overload;

public class Overloads {
    String uniqueID;

    public int addNums(int a, int b) {
        return a + b;
    }

    public double addNums(double a, double b){
        return a + b; //different args and different return type.
    }

    public void setUniqueID(String theID){
        uniqueID = theID;
    }

    public void setUniqueID(int ssNumber){
        String numString = "" + ssNumber;
        setUniqueID(numString);
    }
}

